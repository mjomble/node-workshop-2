Kood koos töötoa käigus tehtud muudatustega:<br>
https://gitlab.com/mjomble/node-workshop-2/-/tree/end-of-workshop

### Töötubade salvestused

#### Esimene töötuba (29\. veebruar 2024)
https://drive.google.com/open?id=1-5CJBsFEQF_abGTzqjHkKdhjUIiwM-wy

#### Teine (käesolev) töötuba (25\. aprill 2024)
https://drive.google.com/open?id=1HIFJbbDCuQfErtQ1RwGCYTyoaf7KbPiH

