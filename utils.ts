import type { HandlerResponse } from './types'

export const createJsonResponse = (body: unknown, statusCode = 200): HandlerResponse => ({
    statusCode,
    headers: {
        'Content-Type': 'application/json',
    },
    body: JSON.stringify(body),
})
