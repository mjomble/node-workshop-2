import type { RequestHandler } from './types'
import { createJsonResponse } from './utils'

export const createHandlers = (): RequestHandler[] => [
    {
        method: 'GET',
        pathPattern: '/hello',
        handle: async () => ({
            statusCode: 200,
            body: 'Hello World',
        }),
    },
    {
        method: 'GET',
        pathPattern: '/json/:foo',
        handle: async ({ pathParams }) => createJsonResponse({
            foo: pathParams.foo,
        }),
    },
    {
        method: 'POST',
        pathPattern: '/json',
        handle: async ({ jsonBody }) => createJsonResponse({ input: jsonBody }),
    },
    {
        handle: async () => ({
            statusCode: 404,
            body: 'Not Found',
        }),
    },
]
