import { request, type IncomingMessage } from 'node:http'

import getRawBody from 'raw-body'

const postJson = async (url: string, reqBody: unknown) => {
    return new Promise<IncomingMessage>((resolve) => {
        const req = request(url, {
            method: 'POST',
            headers: {
                'content-type': 'application/json',
            },
        })

        req.write(JSON.stringify(reqBody))
        req.on('response', (res) => resolve(res))
        req.end()
    })
}

const run = async () => {
    const res = await postJson('http://localhost:3000/users', {
        first_name: 'Marco',
        last_name: 'Tasane',
    })

    console.log('Server responded with status code', res.statusCode)
    console.log('Response body:')

    const resBody = await getRawBody(res)
    console.log(resBody.toString())
}

run().catch((error) => console.error(error))
