import http from 'node:http'

import { applyResponse } from './apply-response'
import { handleRequest } from './handle-request'
import { createHandlers } from './handlers'

const run = async () => {
    const handlers = createHandlers()

    const server = http.createServer(async (req, res) => {
        const handlerResponse = await handleRequest(req, handlers)
        applyResponse(handlerResponse, res)
    })

    server.listen(3000, () => {
        console.log('Server is running on http://localhost:3000')
    })
}

run().catch((error) => console.error(error))
