import type { IncomingMessage } from 'node:http'

import { match as createPathMatcher, pathToRegexp } from 'path-to-regexp'
import getRawBody from 'raw-body'

import type { RequestHandler, HandlerResponse } from './types'

export const handleRequest = async (
    req: IncomingMessage,
    handlers: RequestHandler[],
): Promise<HandlerResponse> => {
    try {
        const url = new URL(req.url ?? '', 'http://base-url')

        for (const handler of handlers) {
            if (reqMatchesHandler(handler, req, url)) {
                // Must await for try-catch to work
                return await executeHandler(handler, req, url)
            }
        }

        return {
            statusCode: 404,
            body: 'Not Found',
        }
    } catch (error) {
        console.error(error)

        return {
            statusCode: 500,
            body: 'Internal Server Error',
        }
    }
}

const reqMatchesHandler = (
    handler: RequestHandler,
    req: IncomingMessage,
    url: URL,
): boolean => {
    if (handler.method && req.method !== handler.method) {
        return false
    }

    if (handler.pathPattern) {
        const regex = pathToRegexp(handler.pathPattern)

        if (!regex.test(url.pathname)) {
            return false
        }
    }

    return true
}

const executeHandler = async (
    handler: RequestHandler,
    req: IncomingMessage,
    url: URL,
): Promise<HandlerResponse> => {
    const pathParams = getPathParams(handler.pathPattern, url)
    const jsonBody = await getJsonBody(req)
    return handler.handle({ req, url, pathParams, jsonBody })
}

/**
 * Example:
 * pathPattern:  /organizations/:organizationId/users/:userId
 * url.pathname: /organizations/123/users/456
 * result:       { organizationId: '123', userId: '456' }
 */
const getPathParams = (
    pathPattern: string | undefined,
    url: URL,
): Record<string, string> => {
    if (!pathPattern) {
        return {}
    }

    const pathMatcher = createPathMatcher(pathPattern, {
        decode: decodeURIComponent,
    })

    const pathResult = pathMatcher(url.pathname)
    return pathResult ? pathResult.params as Record<string, string> : {}
}

const getJsonBody = async (req: IncomingMessage) => {
    const contentType = req.headers['content-type']

    if (!isJson(contentType)) {
        return
    }

    const rawBody = await getRawBody(req)

    try {
        return JSON.parse(rawBody.toString()) as unknown
    } catch (error) {
        console.error('Failed to parse JSON body:', error)
        return
    }
}

const isJson = (contentType: string | undefined) => {
    if (!contentType) {
        return false
    }

    // Input might be something like 'application/json; charset=utf-8'
    // so we need to extract the part before ';'
    const [type] = contentType.split(';')
    return type.toLowerCase() === 'application/json'
}
