import type { ServerResponse } from 'node:http'

import type { HandlerResponse } from './types'

export const applyResponse = (
    response: HandlerResponse,
    res: ServerResponse,
) => {
    try {
        res.statusCode = response.statusCode

        if (response.headers) {
            for (const [key, value] of Object.entries(response.headers)) {
                res.setHeader(key, value)
            }
        }

        if (response.body) {
            if (typeof response.body === 'string') {
                res.end(response.body)
            } else {
                response.body.pipe(res)
            }
        } else {
            res.end()
        }
    } catch (error) {
        res.statusCode = 500
        res.end()
        console.error('Failed to apply response:', error)
    }
}
