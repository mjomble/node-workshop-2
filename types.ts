import type { IncomingMessage } from 'node:http'

import type { SendStream } from 'send'

export interface RequestHandler {
    method?: 'GET' | 'POST' | 'PUT' | 'PATCH' | 'DELETE'
    pathPattern?: string
    handle: (params: HandlerParams) => Promise<HandlerResponse>
}

export interface HandlerParams {
    req: IncomingMessage
    url: URL
    pathParams: Record<string, string>
    jsonBody?: unknown
}

export interface HandlerResponse {
    statusCode: number
    headers?: Record<string, string>
    body?: string | SendStream
}
